let grades = [98.5, 94.3, 89.2, 90.1];

console.log(grades[0]);

/*
	An object is similar to an array. It is a collection and/or functionalities. Usually it represent real-world objects.
*/

let grade = {
	// Object initializer / literal notation - objecs consists of properties, which are used to describe an object. Key-value pair.
	math: 89.2,
	english: 98.5,
	science: 90.1,
	filipino: 94.3
};

console.log(grade);
/*
	We use dot-notation to access the properties/values of our object.
*/
console.log(grade.english);

/*
	Syntax:
	let objectName = {
		keyA: valueA,
		keyB: valueB,
		keyC: valueC,
		keyD: valueD,
		.
		.
		.
	}
*/

let cellphone = {
	brandName: "Nokia 3310",
	color: "Dark Blue",
	manufactureDate: 1999
};

console.log(typeof cellphone);

let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 09171234567,
	location: {
		city: "Tokyo",
		country: "Japan"
	},
	emails: [
		"john@mail.com",
		"johnsmith@mail.xyz"
	],
	// Object method - function inside an object.
	fullName: function(){
		return `${this.firstName} ${this.lastName}`;
	}
}
// Dot-notation
console.log(student.location.city);


// Bracket notation
console.log(student["firstName"]);

console.log(student.emails);
console.log(student.emails[1]);

console.log(student.fullName());

// sample for 'this'
const person1 = {
	name: "Jane",
	greeting: function(){
		return `Hi! I'm ${this.name}`;
	}
};

console.log(person1.greeting());

// Array of objects
let contactList = [
	{
		firstName: "John",
		lastName: "Smith",
		location: "Japan",
	},
	{
		firstName: "Jane",
		lastName: "Smith",
		location: "Japan",
	},
	{
		firstName: "Jasmine",
		lastName: "Smith",
		location: "Japan",
	}
];

console.log(contactList[0].firstName);

let people = [
	{
		name:"Juanita",
		age:13
	},{
		name:"Juanito",
		age:14
	}
];

people.forEach(function(person){
	console.log(person.name);
});

console.log(`${people[0].name} are the list`);

// Creating objects using a constructor function (JS object constructors / objects from blueprints)

// Creates a reusable to create several objects that have the same data structure.
// This is useful for creating multiple copies/instances of an object.
/*
	Object literals
	let object = {};

	Instance - distinct/unique objects.
	let object = new Object
*/
/*
	An instance is a concrete occurence of any object which emphasize on the unique identity of it.

	Syntax:

	function objectName(keyA, keyB...){
		this.keyA = keyA,
		this.keyB = keyB
		.
		.
		.
	}
*/

function Laptop(name, manufactureDate){
	// The "this" keyword allows us to assign a new object's property by associating them with values received from our parameter.
	this.name = name;
	this.manufactureDate = manufactureDate;
}

// This is a unique instance of a laptop object.

let laptop = new Laptop("Lenovo", 2008);
console.log(`Result from craeting objects using object constructors`);
console.log(laptop);


// This is another unique instance of the laptop object

let myLaptop = new Laptop("Macbook Air", 2020);
console.log(`Result from craeting objects using object constructors`);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Without new keyword");
console.log(oldLaptop);

// Creating empty objects
let computer = {};
let myComputer = new Object();

console.log(myLaptop.name);
console.log(myLaptop["name"]);

let array = [laptop, myLaptop];
console.log(array[0].name);

// Initializing/Adding/Deleting/Reassigning object properties.

// Initialized / added properties after the object was created.
// This is useful for times when an object's properties are undetermined at the time of creating them.

let car = {};

// Add object properties - we can use dot notation.
car.name = "Honda Civic";
console.log(car);

// Add object properties thru bracket notation
car["manufactureDate"] = 2019;
console.log(car);

// Reassigning object properties
car.name = "Toyota Vios";
console.log(car);

// Deleting object properties
delete car["manufactureDate"];
console.log(`Result from deleting properties thru bracket notation:`);
console.log(car);

delete car.name;
console.log(`Result from deleting properties thru dot notation:`);
console.log(car);

// Object methods
/*
	A method is a function which is a property of an object. They are also functions and one of the key differences they have is that methods are functions related to a specific object.
*/

let person = {
	name: "John",
	talk: function(){
		console.log(`Hello, my name is ${this.name}.`);
	}
}

console.log(person);
console.log(`Result from object methods:`);
person.talk();

// Adding methods to object person.

person.walk = function(){
	console.log(`${this.name} walked 25 steps forward.`);
};
person.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address:{
		city: "Austin",
		country: "Texas"
	},
	email: ["joe@mail.com", "joesmith@mail.xyz"],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}.`)
	}
}

friend.introduce();

/*
	Real-world application of objects.

	Scenario:
		1. We would like to create a game that would have several pokemon interact with each other.
		2. Every pokemon would have the same set of stats, properties, and functions.
*/

// Using object literals to create multiple kinds of pokemon would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(`This pokemon tackled target pokemon`);
		console.log(`targetPokemon's health is now reduced to _targetPokemonHealth_`);s
	},
	faint: function(){
		console.log(`Pokemon fainted`);
	}
}

// Creating an object constructor instead of object literals.

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to _targetPokemonHealth_`);
	};
	this.faint = function(){
		console.log(`${this.name} fainted.`);
	};
}

let pikachu = new Pokemon("Pikachu",16);
let charizard = new Pokemon("Charizard", 8);

pikachu.tackle(charizard);