function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		if(this.health <= 0){
			console.log(`You cannot use ${this.name} because it fainted.`);
		}
		else{
			if(target.health >= 0){
				target.health -= this.attack;
				console.log(`${this.name} tackled ${target.name}`);
				console.log(`${target.name} is now reduced to ${target.health}`);
				if(target.health <= 0){
					target.faint();
				}
			}
			else{
				console.log(`${target.name} has already fainted.`);
			}
		}
	};
	this.faint = function(){
		console.log(`${this.name} has fainted.`);
	};
}

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	}
}

trainer.talk = function(){
	console.log("Pikachu, I choose you!");
};

console.log(trainer);

console.log(`Result of dot notation`);
console.log(`${trainer.name}`);
console.log(`Result of bracket notation`);
console.log(`${trainer["pokemon"]}`);
trainer.talk();

let pikachu = new Pokemon("Pikachu",16);
let charizard = new Pokemon("Charizard", 36);
let bulbasaur = new Pokemon("Bulbasaur", 12);
let zubat = new Pokemon("Zubat", 20);
let deoxys = new Pokemon("Deoxys", 200);
let mewtwo = new Pokemon("Mewtwo", 30);

pikachu.tackle(charizard);
pikachu.tackle(bulbasaur);
pikachu.tackle(zubat);
pikachu.tackle(deoxys);
pikachu.tackle(mewtwo);

charizard.tackle(pikachu);
charizard.tackle(bulbasaur);
charizard.tackle(zubat);
charizard.tackle(deoxys);
charizard.tackle(mewtwo);

zubat.tackle(pikachu);
zubat.tackle(bulbasaur);
zubat.tackle(charizard);
zubat.tackle(deoxys);
zubat.tackle(mewtwo);

mewtwo.tackle(pikachu);
mewtwo.tackle(bulbasaur);
mewtwo.tackle(charizard);
mewtwo.tackle(deoxys);
mewtwo.tackle(mewtwo);

deoxys.tackle(pikachu);
deoxys.tackle(bulbasaur);
deoxys.tackle(charizard);
deoxys.tackle(zubat);
deoxys.tackle(mewtwo);
